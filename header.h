#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <netinet/ether.h>
#include <linux/if_packet.h>
#include <string.h>
#include <sys/ioctl.h>
#include <stdio.h>

#define ETH_NAME "eth1"

int tx_len = 0;
char sendbuf[1024];
char source_ip[16], dest_ip[16];
int sockfd;

// $$$$$$$$$$$$$$$$$$$$$$$$$$$$
// Set switch MAC address here!!!
// $$$$$$$$$$$$$$$$$$$$$$$$$$$$

#define MY_DEST_MAC0	0x52
#define MY_DEST_MAC1	0x54
#define MY_DEST_MAC2	0x00
#define MY_DEST_MAC3	0x12
#define MY_DEST_MAC4	0x35
#define MY_DEST_MAC5	0x02


unsigned short csum(unsigned short *buf, int nwords)
{
    unsigned long sum;
    for(sum=0; nwords>0; nwords--)
        sum += *buf++;
    sum = (sum >> 16) + (sum &0xffff);
    sum += (sum >> 16);
    return (unsigned short)(~sum);
}


struct iphdr * change_ip_header(struct iphdr * iph) {
	iph->ihl = 5;
	iph->version = 4;
	iph->tos = 16; // Low delay
	iph->id = htons(54321);
	iph->ttl = 5; //  keeping dummy value as 5 hops --> Change this value...Decreament it.
	iph->protocol = 17; // UDP
	// Source IP Addr
	iph->saddr = inet_addr(source_ip);
	/* Destination IP address */
	iph->daddr = inet_addr(dest_ip);
	iph->check = 0;
	tx_len += sizeof(struct iphdr);
	return iph;
}

void get_mac_address(struct ifreq * if_mac) {
	int fd;
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	if_mac->ifr_addr.sa_family = AF_INET;
	memset(if_mac, 0, sizeof(struct ifreq));
	strncpy(if_mac->ifr_name, ETH_NAME, IFNAMSIZ-1);
	if (ioctl(fd, SIOCGIFHWADDR, if_mac) < 0)
		perror("SIOCGIFHWADDR");
	close(fd);
}


struct ether_header * change_ethernet_header(struct ether_header *eh, struct ifreq *if_mac) {
	/* Ethernet header */
	eh->ether_shost[0] = ((uint8_t *)&if_mac->ifr_hwaddr.sa_data)[0];
	eh->ether_shost[1] = ((uint8_t *)&if_mac->ifr_hwaddr.sa_data)[1];
	eh->ether_shost[2] = ((uint8_t *)&if_mac->ifr_hwaddr.sa_data)[2];
	eh->ether_shost[3] = ((uint8_t *)&if_mac->ifr_hwaddr.sa_data)[3];
	eh->ether_shost[4] = ((uint8_t *)&if_mac->ifr_hwaddr.sa_data)[4];
	eh->ether_shost[5] = ((uint8_t *)&if_mac->ifr_hwaddr.sa_data)[5];
	eh->ether_dhost[0] = MY_DEST_MAC0;
	eh->ether_dhost[1] = MY_DEST_MAC1;
	eh->ether_dhost[2] = MY_DEST_MAC2;
	eh->ether_dhost[3] = MY_DEST_MAC3;
	eh->ether_dhost[4] = MY_DEST_MAC4;
	eh->ether_dhost[5] = MY_DEST_MAC5;
	eh->ether_type = htons(ETH_P_IP);
	tx_len += sizeof(struct ether_header);
	return eh;
}


struct udphdr * change_udp_header(struct udphdr *udph) {
	/* UDP Header */
	int n;
	udph->source = htons(3423); //  $$$ Set source port to something
	udph->dest = htons(5342); // $$$ Set destport to something
	udph->check = 0; // skip
	tx_len += sizeof(struct udphdr);
	/* Packet data $$$ Payload */
	strncpy(sendbuf + tx_len, "1", strlen("1")); // $$$ SET PMU ID here!!!! // This is the Payload
	tx_len += strlen("1");
	n = tx_len - sizeof(struct ether_header) - sizeof(struct iphdr);
	udph->len = htons(sizeof(struct udphdr));
	udph->check = csum((unsigned short *)(udph), sizeof(struct udphdr)/2);
}
