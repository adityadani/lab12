#include "header.h"
#include <errno.h>


int main(int argc, char **argv) {

	struct ether_header *eh = (struct ether_header *)sendbuf;
	struct iphdr *iph = (struct iphdr *)(sendbuf + sizeof(struct ether_header));	
	struct udphdr *udph = (struct udphdr *)(sendbuf + sizeof(struct ether_header) + sizeof(struct iphdr));
	struct ifreq if_mac;
	struct sockaddr_in sin;
	socklen_t len;
	int n, ni, ne, nu, socknew;

	printf("\n I am here %s %s", argv[1], argv[2]);

	strcpy(source_ip, argv[1]);
	strcpy(dest_ip, argv[2]);
	

	sin.sin_family = AF_INET;
	// Source port, can be any, modify as needed
	sin.sin_port = htons(12345);
	// Source IP, can be any, modify as needed
	sin.sin_addr.s_addr = inet_addr(argv[2]);

	
	/* Open RAW socket to send on */
	if ((socknew = socket(AF_PACKET, SOCK_RAW, IPPROTO_RAW)) == -1) {
		perror("socket");
	}



	get_mac_address(&if_mac);
	change_ethernet_header(eh, &if_mac);
	iph = change_ip_header(iph);
	udph = change_udp_header(udph);

	/* Length of UDP payload and header */
	//udph->len = htons(n);
	//n = htons(n);
	/* Length of IP payload and header */
	iph->tot_len = htons(tx_len - sizeof(struct ether_header));
	/* Calculate IP checksum on completed header */
	iph->check = csum((unsigned short *)(sendbuf+sizeof(struct ether_header)), sizeof(struct iphdr)/2);

	len = sizeof(struct sockaddr_in);


	struct ifreq if_idx;
	memset(&if_idx, 0, sizeof(struct ifreq));
	strncpy(if_idx.ifr_name, ETH_NAME, IFNAMSIZ-1);
	if (ioctl(socknew, SIOCGIFINDEX, &if_idx) < 0)
    	perror("SIOCGIFINDEX");


	/* Destination address */
	struct sockaddr_ll socket_address;
	/* Index of the network device */
	socket_address.sll_ifindex = if_idx.ifr_ifindex;
	/* Address length*/
	socket_address.sll_halen = ETH_ALEN;
	/* Destination MAC */
	socket_address.sll_addr[0] = MY_DEST_MAC0;
	socket_address.sll_addr[1] = MY_DEST_MAC1;
	socket_address.sll_addr[2] = MY_DEST_MAC2;
	socket_address.sll_addr[3] = MY_DEST_MAC3;
	socket_address.sll_addr[4] = MY_DEST_MAC4;
	socket_address.sll_addr[5] = MY_DEST_MAC5;
	/* Send packet */
	if (sendto(socknew, sendbuf, tx_len, 0, (struct sockaddr*)&socket_address, sizeof(struct sockaddr_ll)) < 0)
	    printf("Send failed\n");
	else
		printf("Success");


	printf("\nSend successfull!!!");*/
}
